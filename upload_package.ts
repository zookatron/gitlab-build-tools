import "https://deno.land/x/dotenv@v3.2.0/load.ts";

let [localFilename, remoteFilename] = Deno.args;
if (!localFilename) {
  throw new Error("You must provide the filename of the package file to upload!");
}
if (!remoteFilename) {
  remoteFilename = localFilename;
}

const response = await fetch(
  `${Deno.env.get("CI_API_V4_URL")}/projects/${Deno.env.get("CI_PROJECT_ID")}/packages/generic/${Deno.env.get("CI_PROJECT_NAME")}/${
    Deno.env.get("CI_COMMIT_TAG")
  }/${remoteFilename}`,
  {
    method: "PUT",
    body: await Deno.readFile(localFilename),
    headers: { "JOB-TOKEN": Deno.env.get("CI_JOB_TOKEN") || "" },
  },
);

if (!response.ok) {
  throw new Error(`Unable to upload package file: HTTP ${response.status} ${response.statusText}: ${await response.text()}`);
}

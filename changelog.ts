const response = await fetch(`${Deno.env.get("CI_API_V4_URL")}/projects/${Deno.env.get("CI_PROJECT_ID")}/releases/permalink/latest`, {
  headers: { "JOB-TOKEN": Deno.env.get("CI_JOB_TOKEN") || "" },
});
const json = await response.json();
const process = Deno.run({
  cmd: ["git", "log", `${json.name}..${Deno.env.get("CI_COMMIT_SHA")}`, "--format=%s"],
  stdout: "piped",
  stderr: "piped",
});
const output = new TextDecoder().decode(await process.output());
process.close();
console.log(output.split("\n").filter((line) => line).map((line) => `- ${line}`).join("\n"));

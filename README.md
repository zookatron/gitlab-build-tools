# GitLab Build Tools

This is a collection of tools for GitLab CI builds. The tools assume you are using a tag-based release system where CI jobs are triggered by
pushing a new tag ot the Git repository.

## Changelog Generator

The changelog.ts file is a Deno script for automatically generating a changelog for a GitLab Release. It pulls all the commit messages for
the commits since the previous release and formats them using Markdown.

Example usage:

```shell
deno run --allow-env --allow-net --allow-run https://gitlab.com/zookatron/gitlab-build-tools/-/raw/main/changelog.ts
```

Outputs:

```md
- Commit message 1
- Commit message 2
- Commit message 3
```

## Package Uploader

The upload_package.ts file is a Deno script for uploading a new package file to the GitLab generic package registry. It accepts two
arguments, the first is the path to the file on the local filesystem to upload, and the second is the path that should be used for the
uploaded file once it is uploaded to GitLab. The value for the second argument will default to the value of the first argument if it is not
provided.

Example usage:

```shell
deno run --allow-env --allow-net https://gitlab.com/zookatron/gitlab-build-tools/-/raw/main/upload_package.ts ${CI_PROJECT_NAME}
```
